(function ($) {

  Drupal.behaviors.floaty = {
    attach: function (context, settings) {

      $('.floaty:not(.bound)').bind('mousedown', function (e) {
        $('#edit-target').val($(this).attr('id'));
        e.stopPropagation();
        return false;
      });

      $('.floaty:not(.bound)').addClass('bound');
      $('.floaty-block-mask:not(.bound)', context).bind('mousedown', start_drag);
      $('.floaty-block-mask:not(.bound)').addClass('bound');

      var dragging = null;
      var mode = 'move';
      var source;
      var snap = true;
      var grid_size = 20;

      function start_drag(e) {
        mode = 'move';
        dragging = $(this).parent();
        dragging_x = e.pageX;
        dragging_y = e.pageY;
        source = {
          x: parseInt(dragging.css('left')),
          y: parseInt(dragging.css('top'))
        };
        $(document).bind('mouseup', release_drag);
        $(document).bind('mousemove', move_drag);
        var grid = $(document.createElement('div'));
        grid.addClass('floaty-grid');
        var offset = $(this).parents('.floaty.anchor').offset();
        console.log(offset);
        offset.left = offset.left - (Math.round(offset.left / grid_size) * grid_size);
        offset.top = offset.top - (Math.round(offset.top / grid_size) * grid_size);
        grid.css('backgroundPosition', '' + offset.left + 'px ' + offset.top + 'px');
        $('body').prepend(grid);
        $('.floaty-grid').css('left', 0);
        $('.floaty-grid').css('top', 0);
        $('.floaty-grid').css('width', $(document).width());
        $('.floaty-grid').css('height', $(document).height());
        e.stopPropagation();
        return false;
      }

      function move_drag(e) {
        switch (mode) {
          case 'move':
            source.x = source.x - (dragging_x - e.pageX);
            source.y = source.y - (dragging_y - e.pageY);
            if (snap) {
              dragging.css('left', '' + (Math.round(source.x / grid_size) * grid_size) + 'px');
              dragging.css('top', '' + (Math.round(source.y / grid_size) * grid_size) + 'px');
            } else {
              dragging.css('left', '' + (source.x) + 'px');
              dragging.css('top', '' + (source.y) + 'px');
            }
            dragging_x = e.pageX;
            dragging_y = e.pageY;
            break;

          case 'size':
            break;
        }

        e.stopPropagation();
        return false;
      }

      function release_drag(e) {
        $(document).unbind('mousemove', move_drag);
        $(document).unbind('mouseup', release_drag);
        $('.floaty-grid').remove();

        anchor = dragging.parents('.floaty.anchor').attr('id');
        id = dragging.attr('id');
        offset = dragging.position();

        data = {
          anchor: anchor,
          id: id,
          left: offset.left,
          top: offset.top
        };

        $.ajax({
          url: '/floaty/block',
          type: 'POST',
          dataType: "json",
          data: data,
          success: function (data) {
          }
        });

        dragging = null;
      }
    }
  };

})(jQuery);
